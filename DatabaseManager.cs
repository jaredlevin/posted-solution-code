using UnityEngine;
using Amazon;
using Amazon.CognitoIdentity;
using System.Security.Cryptography;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using UnityEngine.Networking;
using ImaginationOverflow.UniversalDeepLinking;
using System.Json;
using Unity.RemoteConfig;
using System;




public class DatabaseManager : MonoBehaviour {

    // constants
    private const string COGNITO_USER_POOL_ID = "us-east-1_xxxxxxxxx";
    private const string COGNITO_DOMAIN = "https://xxxx.auth.us-east-1.amazoncognito.com";
    private const string COGNITO_OAUTH_AUTHROIZE_REDIRCECT_URI = "xxx://attemptsignin";
    private const string COGNITO_USER_POOL_ADDLOGIN_DOMAIN = "cognito-idp.us-east-1.amazonaws.com";
    public string COGNITO_HOSTED_UI;
    
    // read via remote config
    private string COGNITO_USER_POOL_APP_CLIENT_ID;
    private string COGNITO_IDENTITY_POOL_ID;
    private string REGION_ENDPOINT;
    private string DYNAMODB_TABLE;

    // PKCE
    private string code_challenge;
    private string code_verifier;
    public event Action LoginComplete;
    public event Action ReadyForLoginAttempt;
    public event Action UserDataLoaded;
    public event Action UserDataSaving;


    public bool isLoggedIn = false;
    public bool isUserDataLoaded = false;

    // userinfo
    public string email;
    public string email_verified;
    public string username;

    // token info
    private string id_token;
    private string access_token;
    private string refresh_token;
    private string token_type;
    private long expires_in;


    private CognitoAWSCredentials cognitoAWSCredentials;
    private AmazonDynamoDBClient dynamoDBClient;
    private Table jWorld;

    public Document userData {
        get {
            return _userData;
        }
        set {
            _userData = value;
        }
    }

    private Document _userData;
 
    private Error jError;

    // Optionally declare variables for any custom user attributes:
    public struct userAttributes {  }

    // Optionally declare variables for any custom app attributes:
    public struct appAttributes {  }

    void Awake() {
        ConfigManager.FetchCompleted += ApplyRemoteSettings;
    
        // Fetch configuration setting from the remote service: 
        ConfigManager.FetchConfigs<userAttributes, appAttributes>(new userAttributes(), new appAttributes());

    }

    void Start() {
        // AWS init
        UnityInitializer.AttachToGameObject(this.gameObject); // for AWS services
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;	
        DeepLinkManager.Instance.LinkActivated += Instance_LinkActivated;
        jError = GameObject.FindObjectOfType<Error>();
    }

    void ApplyRemoteSettings (ConfigResponse configResponse) {
        // Conditionally update settings, depending on the response's origin:
       
        COGNITO_USER_POOL_APP_CLIENT_ID = ConfigManager.appConfig.GetString ("COGNITO_USER_POOL_APP_CLIENT_ID");
        REGION_ENDPOINT = ConfigManager.appConfig.GetString ("REGION_ENDPOINT");
        COGNITO_IDENTITY_POOL_ID = ConfigManager.appConfig.GetString ("COGNITO_IDENTITY_POOL_ID");
        DYNAMODB_TABLE = ConfigManager.appConfig.GetString ("DYNAMODB_TABLE");

        if (ReadyForLoginAttempt != null) {
            ReadyForLoginAttempt();
        }
    }

    string Base64URlEncode(byte[] bytes) {

        return Convert.ToBase64String(bytes).Replace('+', '-').Replace('/', '_').Replace("=", "");
    }

    string createCodeChallenge() {
        // generate code_verifier 
        RandomNumberGenerator Rng = RandomNumberGenerator.Create();
        var bytes = new byte[32];
        Rng.GetBytes(bytes);
        code_verifier = Base64URlEncode(bytes);

        // create code_challenge
        using (var sha256 = SHA256.Create())
        {
            var challengeBytes = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(code_verifier));
            return Base64URlEncode(challengeBytes);
        }
        
    }



    private void OnDestroy()
    {
        try {
            DeepLinkManager.Instance.LinkActivated -= Instance_LinkActivated;
            
        }
        catch (Exception e)
        {
            jError.handleError(e);
        }
    }


    public void BeginLogin() { 

        // create and save code challenge
        code_challenge = createCodeChallenge();
        var state = Base64URlEncode(Guid.NewGuid().ToByteArray());
        // build hosted UI URL with code_verifier
        COGNITO_HOSTED_UI = COGNITO_DOMAIN + "/oauth2/authorize?response_type=code&client_id=" + COGNITO_USER_POOL_APP_CLIENT_ID + "&state=" + state + "&scope=openid&code_challenge=" + code_challenge + "&code_challenge_method=S256&redirect_uri=" + COGNITO_OAUTH_AUTHROIZE_REDIRCECT_URI;
        
        try {
            isLoggedIn = false;
            jError = GameObject.FindObjectOfType<Error>();
    
        

        }
        catch (Exception e)
        {
            jError.handleError(e);
        }
       Application.OpenURL(COGNITO_HOSTED_UI);
	}


  public void Instance_LinkActivated(LinkActivation s)
    {
        
        try {
        	// this uses the cognito Hosted UI to login, use this code to get a token
            if (!s.QueryString.ContainsKey("code"))
            {
                jError.handleError("No code returned in querystring!");
                return;
            }

            string code = s.QueryString["code"];

            if (!getIdToken(code))
                return; // call web service token endpoint and wait for response
            
            if (!getUserInfo())
                return; // call web service token endpoint and wait for response
         
            // Initialize the Amazon Cognito credentials provider
            cognitoAWSCredentials = new CognitoAWSCredentials(
                COGNITO_IDENTITY_POOL_ID, // Identity pool ID
                RegionEndpoint.GetBySystemName(REGION_ENDPOINT) // Region
            );

            cognitoAWSCredentials.AddLogin(COGNITO_USER_POOL_ADDLOGIN_DOMAIN + "/" + COGNITO_USER_POOL_ID, id_token);

            dynamoDBClient = new AmazonDynamoDBClient(cognitoAWSCredentials, RegionEndpoint.GetBySystemName(REGION_ENDPOINT));
            
      
            
            Table.LoadTableAsync(dynamoDBClient, DYNAMODB_TABLE, loadTableAsync_Callback);




            // fire logged in event
            if (LoginComplete != null)
                LoginComplete();
		
        }
        catch (Exception e)
        {
            jError.handleError(e);
        }
        
    }

    private void loadTableAsync_Callback(AmazonDynamoDBResult<Table> asyncResult) {
        if (asyncResult.Exception != null) {
            jError.handleError(asyncResult.Exception);
        }
        else {
            jWorld = asyncResult.Result;
            isLoggedIn = true;
            loadFromDatabase();
        }

    }
    private bool getIdToken(string code) {
         // now get the token from the authroization code with a POST to toekn endpoint
        WWWForm tokenRequestPostData = new WWWForm();

        tokenRequestPostData.AddField("grant_type", "authorization_code");
        tokenRequestPostData.AddField("client_id", COGNITO_USER_POOL_APP_CLIENT_ID);
        tokenRequestPostData.AddField("code", code);
        tokenRequestPostData.AddField("code_verifier", code_verifier);
        tokenRequestPostData.AddField("redirect_uri", COGNITO_OAUTH_AUTHROIZE_REDIRCECT_URI);

        UnityWebRequest tokenRequest = UnityWebRequest.Post(COGNITO_DOMAIN + "/oauth2/token", tokenRequestPostData);
        tokenRequest.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        tokenRequest.SendWebRequest();
        while (!tokenRequest.isDone) { }
        
        if (tokenRequest.isNetworkError || tokenRequest.isHttpError)
        {
            Debug.Log(tokenRequest.error);
            return false;
        }
        else
        {
            var tokenResponseParse = JsonObject.Parse(tokenRequest.downloadHandler.text);
            id_token = tokenResponseParse["id_token"];
            access_token = tokenResponseParse["access_token"];
            refresh_token = tokenResponseParse["refresh_token"];
            token_type = tokenResponseParse["token_type"];
            expires_in = tokenResponseParse["expires_in"];
            return true;
        }

    }
    private bool getUserInfo() {
        UnityWebRequest tokenRequest = UnityWebRequest.Get(COGNITO_DOMAIN + "/oauth2/userInfo");
        tokenRequest.SetRequestHeader("Authorization", "Bearer " + access_token);
        tokenRequest.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        tokenRequest.SendWebRequest();
        while (!tokenRequest.isDone) { }
        
        if (tokenRequest.isNetworkError || tokenRequest.isHttpError)
        {
            jError.handleError(tokenRequest.error);
            return false;
        }
        else
        {
            var tokenResponseParse = JsonObject.Parse(tokenRequest.downloadHandler.text);
            email = tokenResponseParse["email"];
            email_verified  = tokenResponseParse["email_verified"];
            username  = tokenResponseParse["username"];

            if (email_verified.ToLower() != "true")
            {
                Debug.Log("Email not verified");
                return false;
            }
            return true;    
        }

    }
    

 
   
    public void saveToDatabase() { 
        try 
        {
            if (!isLoggedIn)
                return;
            
            if (UserDataSaving != null)
                UserDataSaving();

            
            _userData["userID"] = username;
            _userData["userEmail"] = email;
            _userData["lastUpdated"] = DateTime.UtcNow;

            jWorld.UpdateItemAsync(_userData,(r)=>{
                if (r.Exception != null)
                    jError.handleError(r.Exception);

            });
	    }
        catch (Exception e)
        {
            jError.handleError(e);
        }
    }

    
    public void loadFromDatabase() { 
        try 
        {
            if (!isLoggedIn)
                return;

        

            jWorld.GetItemAsync(username, (r)=>{
                if (r.Exception != null)
                    jError.handleError(r.Exception);
                else {
                    if (r.Result == null)
                        _userData = new Document();
                    else
                        _userData = r.Result;
                    isUserDataLoaded = true;
                    if (UserDataLoaded != null)
                        UserDataLoaded();
                }

            });

            
	    }
        catch (Exception e)
        {
            jError.handleError(e);
            return;
        }
    }

   
}

