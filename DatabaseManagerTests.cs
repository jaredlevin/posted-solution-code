
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium;
using ImaginationOverflow.UniversalDeepLinking;
using System;
using NSubstitute;
using Unity.RemoteConfig;
using Amazon.DynamoDBv2.DocumentModel;

namespace Tests
{
    public class DatabaseManagerTests
    {
        INavigateURL navigateURLSubstitute;

        IWebDriver driver;

        DatabaseManager databaseManager;

        [UnityTest]
        public IEnumerator LoginTest()
        {
            navigateURLSubstitute = Substitute.For<INavigateURL>();
            navigateURLSubstitute.When(x => x.navigateURL(Arg.Any<string>())).Do(x => subNavigateURL((string) x.Args()[0]));

            // mock remote config
            IReadRemoteConfig readRemoteConfigMock = Substitute.For<IReadRemoteConfig>();
            RemoteConfigData remoteConfigDataMock = new RemoteConfigData();

            // return these set values, do not grab remote config
            remoteConfigDataMock.COGNITO_IDENTITY_POOL_ID = Environment.GetEnvironmentVariable("COGNITO_IDENTITY_POOL_ID");
            remoteConfigDataMock.COGNITO_USER_POOL_APP_CLIENT_ID = Environment.GetEnvironmentVariable("COGNITO_USER_POOL_APP_CLIENT_ID");
            remoteConfigDataMock.DYNAMODB_TABLE = Environment.GetEnvironmentVariable("DYNAMODB_TABLE");
            remoteConfigDataMock.REGION_ENDPOINT = Environment.GetEnvironmentVariable("REGION_ENDPOINT");
            readRemoteConfigMock.getRemoteConfigData().Returns(remoteConfigDataMock);
            
            // return these set values, do not grab remote config
            readRemoteConfigMock.When(x => x.FetchConfig()).Do(x =>  { return; } );
            readRemoteConfigMock.When(x => x.ApplyRemoteSettings(Arg.Any<ConfigResponse>())).Do(x =>  { 
                return; 
                } );
            
            // read our test username and password
            string testUsername = Environment.GetEnvironmentVariable("testUsername");
            string testPassword = Environment.GetEnvironmentVariable("testPassword");
            databaseManager = new GameObject().AddComponent<DatabaseManager>();
            databaseManager.readRemoteConfig = readRemoteConfigMock;
            databaseManager.navigateURLInterface = navigateURLSubstitute;
            Assert.IsFalse(databaseManager.isLoggedIn());
            
            databaseManager.BeginLogin();
            
            yield return LoginAsTestUser(databaseManager, testUsername, testPassword);
    
            // wait 3 seconds
            yield return new WaitForSecondsRealtime(3);
            Assert.IsTrue(databaseManager.isLoggedIn());
            var username = databaseManager.getUserProperties().username;
            Assert.AreEqual(testUsername, username);
        }

        public void subNavigateURL(string url) {
            driver = new SafariDriver();
            driver.Url = url;
            driver.Navigate();
        }
        public IEnumerator LoginAsTestUser(DatabaseManager databaseManager, string testUsername, string testPassword) {
            
            IWebElement elementUsername = driver.FindElement(By.Id("signInFormUsername"));
   
            elementUsername.SendKeys(testUsername);
            yield return new WaitForSecondsRealtime(1);
            IWebElement elementPassword = driver.FindElement(By.Id("signInFormPassword"));
   
            elementPassword.SendKeys(testPassword);
            elementPassword.Submit();
            yield return new WaitForSeconds(3);
            if (!driver.Url.StartsWith("jworld://"))
            {
                if (driver != null) {
                    driver.Quit();
                    driver = null;
                }
                Assert.Fail("Not redirected to jworld://");
            }

            string queryStringAsString = driver.Url.Split('?')[1];
            Dictionary<string, string> queryStringAsDictionary = new Dictionary<string, string>();
            string[] nameValuePairs = queryStringAsString.Split('&');
            foreach (string nameValuePair in nameValuePairs)
            {
                queryStringAsDictionary.Add(nameValuePair.Split('=')[0], nameValuePair.Split('=')[1]);
            }
            string url = driver.Url;
            if (driver != null) {
                driver.Quit();
                driver = null;
            }
        
            LinkActivation linkActivation = new LinkActivation(url, queryStringAsString, queryStringAsDictionary);
            databaseManager.Instance_LinkActivated(linkActivation);
            yield return new WaitForFixedUpdate();
        }
        
        [UnityTest]
        public IEnumerator LoadFromDatabase_test()
        {
            yield return LoginTest();
    
            databaseManager.UserDataLoaded += delegate()
            {
                 Assert.Pass();
            };

            databaseManager.loadFromDatabase();

             // wait 3 seconds for event to be called, if not called by then fail
            yield return new WaitForSecondsRealtime(3);
            
        }

        void UserDataLoaded_Handler() {
            Assert.Pass();
        }

        [UnityTest]
        public IEnumerator SaveLoadToDatabase_test()
        {
            yield return LoginTest();

            databaseManager.UserDataSaved += delegate()
            {
                 Assert.Pass();
            };
    
            databaseManager.saveToDatabase();


            // wait 3 seconds for event to be called, if not called by then fail
            yield return new WaitForSecondsRealtime(3);
        }
    }
}
