# Posted Solution:  How do I use the AWS Mobile SDK in Unity3d games to connect to AWS DynamoDB without storing access keys in code or exposing the token to the end user?

## Creating a Secure Unity3d Game
Here is an example of using AWS Cognito to obtain temporary credentials that can be used to read and write to a DynamoDB in Unity3d. The user must create an account with a password and validate their email. This uses the authorization code grant workflow along with PKCE, so no OAUTH client secrets or AWS keys are needed in the compiled application.  It is super-secure, the token is never exposed to the user and is not stored in PlayerPrefs. We use the mobile device’s browser along with Cognito Hosted UI for sign up and log in.  Since the workflow requires a custom URL scheme, the Imagination Overflow Universal Deep Linking asset store asset is used to register the custom URL scheme.

If this helps you, drop me a line.  I love experimenting with AWS and building the most secure solution, and who doesn’t like working in Unity3d  ;)

## Demo
Only the Login/Sign up button is enabled until the user has received valid AWS credentials to read from the DynamoDB database.  Once the user has signed in, the Player Setup and Play! buttons are enabled.  A screen grab of both the Unity editor and iPhone compiled app are shown.

### DatabaseManager.cs
This is the main Unity code script. Using AWS Mobile SDK for Unity, it handles authenticating the user and obtaining the user credentials, username and email for use with the DynamoDB Unity3d objects.  The callback URL (after a successful sign in) contains a custom scheme (not http:// or https://) so that the mobile app is opened. This is where the Deep Linking Asset is needed.

### Imagination Overflow Universal Deep Linking Asset
https://assetstore.unity.com/packages/tools/integration/universal-deep-linking-seamless-deep-link-and-web-link-associati-125172

### AWS Mobile SDK for Unity
https://docs.aws.amazon.com/mobile/sdkforunity/developerguide/setup-unity.html

### Cognito Hosted UI
https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-app-integration.html

### User Pool Authorization Code Grant Workflow
https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-authentication-flow.html

### AWS TOKEN Endpoint
https://docs.aws.amazon.com/cognito/latest/developerguide/token-endpoint.html

### PKCE
https://aws.amazon.com/blogs/mobile/understanding-amazon-cognito-user-pool-oauth-2-0-grants/
